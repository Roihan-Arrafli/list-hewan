 const listHewan = [
 {
    namahewan: "Zebra",
    jenis: "Herbivora"
 },
 {
    namahewan: "Singa",
    jenis: "Carnivora"
 },
 {
    namahewan: "Harimau",
    jenis: "Carnivora"
 },
 {
    namahewan: "Serigala",
    jenis: "Carnivora"
 },
 {
    namahewan: "Domba",
    jenis: "Herbivora"
 },
 {
    namahewan: "Ikan Hiu",
    jenis: "Carnivora"
 }
 ];

// tampil semua
 for(let i = 0; i <listHewan.length; i++){
        console.log(listHewan[i]["namahewan"], ("adalah Jenis Hewan " + listHewan[i]["jenis"]));
 }

 console.log("-cari-")
// berdasarkan data yg dicari
function cariNamaHewan(namahewan) {
    for(let i = 0; i <listHewan.length; i++){
        if (listHewan[i]["namahewan"] == namahewan) {
            console.log(listHewan[i]["namahewan"], 
            ("adalah jenis hewan " +listHewan[i]["jenis"]));
        }
    }
}
cariNamaHewan("Singa")

console.log("-filter-")
// filter berdasarkan jenis
function filterJenis(jenis) {
    for(let i = 0; i <listHewan.length; i++){
        if (listHewan[i]["jenis"] == jenis) {
            console.log(listHewan[i]["namahewan"]);
        }
    }
}
filterJenis("Herbivora")